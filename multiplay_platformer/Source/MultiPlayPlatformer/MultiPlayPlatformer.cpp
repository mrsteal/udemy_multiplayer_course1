// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "MultiPlayPlatformer.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MultiPlayPlatformer, "MultiPlayPlatformer" );
 