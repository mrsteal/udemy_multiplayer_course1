// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MULTIPLAYPLATFORMER_MultiPlayPlatformerGameMode_generated_h
#error "MultiPlayPlatformerGameMode.generated.h already included, missing '#pragma once' in MultiPlayPlatformerGameMode.h"
#endif
#define MULTIPLAYPLATFORMER_MultiPlayPlatformerGameMode_generated_h

#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_SPARSE_DATA
#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_RPC_WRAPPERS
#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMultiPlayPlatformerGameMode(); \
	friend struct Z_Construct_UClass_AMultiPlayPlatformerGameMode_Statics; \
public: \
	DECLARE_CLASS(AMultiPlayPlatformerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MultiPlayPlatformer"), MULTIPLAYPLATFORMER_API) \
	DECLARE_SERIALIZER(AMultiPlayPlatformerGameMode)


#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMultiPlayPlatformerGameMode(); \
	friend struct Z_Construct_UClass_AMultiPlayPlatformerGameMode_Statics; \
public: \
	DECLARE_CLASS(AMultiPlayPlatformerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MultiPlayPlatformer"), MULTIPLAYPLATFORMER_API) \
	DECLARE_SERIALIZER(AMultiPlayPlatformerGameMode)


#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MULTIPLAYPLATFORMER_API AMultiPlayPlatformerGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMultiPlayPlatformerGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MULTIPLAYPLATFORMER_API, AMultiPlayPlatformerGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMultiPlayPlatformerGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MULTIPLAYPLATFORMER_API AMultiPlayPlatformerGameMode(AMultiPlayPlatformerGameMode&&); \
	MULTIPLAYPLATFORMER_API AMultiPlayPlatformerGameMode(const AMultiPlayPlatformerGameMode&); \
public:


#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MULTIPLAYPLATFORMER_API AMultiPlayPlatformerGameMode(AMultiPlayPlatformerGameMode&&); \
	MULTIPLAYPLATFORMER_API AMultiPlayPlatformerGameMode(const AMultiPlayPlatformerGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MULTIPLAYPLATFORMER_API, AMultiPlayPlatformerGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMultiPlayPlatformerGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMultiPlayPlatformerGameMode)


#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_9_PROLOG
#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_SPARSE_DATA \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_RPC_WRAPPERS \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_INCLASS \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_SPARSE_DATA \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_INCLASS_NO_PURE_DECLS \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MULTIPLAYPLATFORMER_API UClass* StaticClass<class AMultiPlayPlatformerGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
