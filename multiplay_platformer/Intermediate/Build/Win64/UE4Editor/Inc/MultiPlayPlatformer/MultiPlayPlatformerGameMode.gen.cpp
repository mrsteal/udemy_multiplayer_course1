// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MultiPlayPlatformer/MultiPlayPlatformerGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMultiPlayPlatformerGameMode() {}
// Cross Module References
	MULTIPLAYPLATFORMER_API UClass* Z_Construct_UClass_AMultiPlayPlatformerGameMode_NoRegister();
	MULTIPLAYPLATFORMER_API UClass* Z_Construct_UClass_AMultiPlayPlatformerGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_MultiPlayPlatformer();
// End Cross Module References
	void AMultiPlayPlatformerGameMode::StaticRegisterNativesAMultiPlayPlatformerGameMode()
	{
	}
	UClass* Z_Construct_UClass_AMultiPlayPlatformerGameMode_NoRegister()
	{
		return AMultiPlayPlatformerGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AMultiPlayPlatformerGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMultiPlayPlatformerGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MultiPlayPlatformer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMultiPlayPlatformerGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "MultiPlayPlatformerGameMode.h" },
		{ "ModuleRelativePath", "MultiPlayPlatformerGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMultiPlayPlatformerGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMultiPlayPlatformerGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMultiPlayPlatformerGameMode_Statics::ClassParams = {
		&AMultiPlayPlatformerGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AMultiPlayPlatformerGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMultiPlayPlatformerGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMultiPlayPlatformerGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMultiPlayPlatformerGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMultiPlayPlatformerGameMode, 214413461);
	template<> MULTIPLAYPLATFORMER_API UClass* StaticClass<AMultiPlayPlatformerGameMode>()
	{
		return AMultiPlayPlatformerGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMultiPlayPlatformerGameMode(Z_Construct_UClass_AMultiPlayPlatformerGameMode, &AMultiPlayPlatformerGameMode::StaticClass, TEXT("/Script/MultiPlayPlatformer"), TEXT("AMultiPlayPlatformerGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMultiPlayPlatformerGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
