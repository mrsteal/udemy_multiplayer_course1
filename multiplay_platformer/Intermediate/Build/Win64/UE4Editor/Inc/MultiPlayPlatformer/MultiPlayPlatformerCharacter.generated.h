// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MULTIPLAYPLATFORMER_MultiPlayPlatformerCharacter_generated_h
#error "MultiPlayPlatformerCharacter.generated.h already included, missing '#pragma once' in MultiPlayPlatformerCharacter.h"
#endif
#define MULTIPLAYPLATFORMER_MultiPlayPlatformerCharacter_generated_h

#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_SPARSE_DATA
#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_RPC_WRAPPERS
#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMultiPlayPlatformerCharacter(); \
	friend struct Z_Construct_UClass_AMultiPlayPlatformerCharacter_Statics; \
public: \
	DECLARE_CLASS(AMultiPlayPlatformerCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MultiPlayPlatformer"), NO_API) \
	DECLARE_SERIALIZER(AMultiPlayPlatformerCharacter)


#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMultiPlayPlatformerCharacter(); \
	friend struct Z_Construct_UClass_AMultiPlayPlatformerCharacter_Statics; \
public: \
	DECLARE_CLASS(AMultiPlayPlatformerCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MultiPlayPlatformer"), NO_API) \
	DECLARE_SERIALIZER(AMultiPlayPlatformerCharacter)


#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMultiPlayPlatformerCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMultiPlayPlatformerCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMultiPlayPlatformerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMultiPlayPlatformerCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMultiPlayPlatformerCharacter(AMultiPlayPlatformerCharacter&&); \
	NO_API AMultiPlayPlatformerCharacter(const AMultiPlayPlatformerCharacter&); \
public:


#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMultiPlayPlatformerCharacter(AMultiPlayPlatformerCharacter&&); \
	NO_API AMultiPlayPlatformerCharacter(const AMultiPlayPlatformerCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMultiPlayPlatformerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMultiPlayPlatformerCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMultiPlayPlatformerCharacter)


#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AMultiPlayPlatformerCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AMultiPlayPlatformerCharacter, FollowCamera); }


#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_9_PROLOG
#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_SPARSE_DATA \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_RPC_WRAPPERS \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_INCLASS \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_SPARSE_DATA \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_INCLASS_NO_PURE_DECLS \
	MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MULTIPLAYPLATFORMER_API UClass* StaticClass<class AMultiPlayPlatformerCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MultiPlayPlatformer_Source_MultiPlayPlatformer_MultiPlayPlatformerCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
